<?xml version="1.0"?>

<st-source>
<time-stamp>From VisualWorks® NonCommercial, 7.6 of March 3, 2008 on October 25, 2008 at 10:04:00 pm</time-stamp>
<!-- Package Dictionary Framework* -->


<class>
<name>AbstractDictionary</name>
<environment>Smalltalk</environment>
<super>Core.Object</super>
<private>false</private>
<indexed-type>none</indexed-type>
<inst-vars>names values </inst-vars>
<class-inst-vars></class-inst-vars>
<imports></imports>
<category></category>
<attributes>
<package>Dictionary Framework</package>
</attributes>
</class>

<class>
<name>SmallDictionary</name>
<environment>Smalltalk</environment>
<super>AbstractDictionary</super>
<private>false</private>
<indexed-type>none</indexed-type>
<inst-vars></inst-vars>
<class-inst-vars></class-inst-vars>
<imports></imports>
<category></category>
<attributes>
<package>Dictionary Framework</package>
</attributes>
</class>

<class>
<name>FastDictionary</name>
<environment>Smalltalk</environment>
<super>AbstractDictionary</super>
<private>false</private>
<indexed-type>none</indexed-type>
<inst-vars>size </inst-vars>
<class-inst-vars></class-inst-vars>
<imports></imports>
<category></category>
<attributes>
<package>Dictionary Framework</package>
</attributes>
</class>




<methods>
<class-id>AbstractDictionary class</class-id> <category>test</category>

<body package="Dictionary Framework" selector="test">test
	"FastDictionary test"
	
	| dict |
	dict := self new.
	dict at: #Maan put: 3476.
	dict at: #Phobos put: 22.
	dict at: #Nereide put: 300.
	dict at: #Deimos put: 13.
	dict at: #Jupiter put: 143000.
	dict inspect</body>
</methods>

<methods>
<class-id>AbstractDictionary class</class-id> <category>instantiation</category>

<body package="Dictionary Framework" selector="new">new
	"instantiate new dictionary"
	
	| size |
	size := self minimalSize.
	^super new initialize: size</body>
</methods>

<methods>
<class-id>AbstractDictionary class</class-id> <category>private</category>

<body package="Dictionary Framework" selector="minimalSize">minimalSize
	"implementation dependent initial size"
	
	^0</body>
</methods>


<methods>
<class-id>AbstractDictionary</class-id> <category>statistics</category>

<body package="Dictionary Framework" selector="size">size
	"dictionary size"
	
	self subclassResponsibility</body>
</methods>

<methods>
<class-id>AbstractDictionary</class-id> <category>access</category>

<body package="Dictionary Framework" selector="at:replace:">at: key replace: value
	"replace value in existing pair"
	
	self
		at: key
		replace: value
		notFound: [self error: 'key not found']</body>

<body package="Dictionary Framework" selector="at:put:duplicate:">at: key put: value duplicate: block
	"insert new key / value pair with user error handling"
	
	| index |
	index := self newIndexAtKey: key.
	index = 0
		ifTrue: [block value]
		ifFalse: [values at: index put: value]</body>

<body package="Dictionary Framework" selector="at:replace:notFound:">at: key replace: value notFound: block
	"replace value existing pair with user error handling"
	
	| index |
	index := self indexAtKey: key.
	index = 0
		ifTrue: [block value]
		ifFalse: [values at: index put: value]</body>

<body package="Dictionary Framework" selector="at:notFound:">at: key notFound: block
	"retrieve value corresponding to key, with user error handling"
	
	| index |
	index := self indexAtKey: key.
	index = 0
		ifTrue: [block value]
		ifFalse: [^values at: index]</body>

<body package="Dictionary Framework" selector="at:">at: key
	"retrieve value corresponding to key"
	
	^self
		at: key
		notFound: [self error: 'key not found']</body>

<body package="Dictionary Framework" selector="at:put:">at: key put: value
	"insert new key / value pair"
	
	self
		at: key
		put: value
		duplicate: [self error: 'duplicate key']</body>
</methods>

<methods>
<class-id>AbstractDictionary</class-id> <category>private</category>

<body package="Dictionary Framework" selector="newIndexAtKey:">newIndexAtKey: key
	"locate index for new key in names"
	
	self subclassResponsibility</body>

<body package="Dictionary Framework" selector="indexAtKey:">indexAtKey: key
	"locate index of key in names"
	
	self subclassResponsibility</body>

<body package="Dictionary Framework" selector="initialize:">initialize: size
	"set up implementation"
	
	names := Array new: size.
	values := Array new: size</body>
</methods>


<methods>
<class-id>FastDictionary class</class-id> <category>private</category>

<body package="Dictionary Framework" selector="minimalSize">minimalSize
	"implementation dependent initial size"
	
	^4</body>
</methods>

<methods>
<class-id>FastDictionary class</class-id> <category>instantiation</category>

<body package="Dictionary Framework" selector="new">new
	"instantiate new fast dictionary"
	
	^super new initializeSize</body>
</methods>


<methods>
<class-id>FastDictionary</class-id> <category>private</category>

<body package="Dictionary Framework" selector="initializeSize">initializeSize
	"set initial size to 0"
	
	size := 0</body>

<body package="Dictionary Framework" selector="newIndexAtKey:">newIndexAtKey: key
	"locate index for new key in names"
	
	| index |
	size + 1 = names size ifTrue: [self extend].
	index := key hash \\ names size + 1.
	[(names at: index) = key]
		whileFalse:
			[(names at: index) isNil
				ifTrue:
					[size := size + 1.
					names at: index put: key.
					^index].
			index := index \\ names size + 1].
	^0 i</body>

<body package="Dictionary Framework" selector="indexAtKey:">indexAtKey: key
	"locate index of key in names"
	
	| index |
	index := key hash \\ names size + 1.
	[(names at: index) = key]
		whileFalse:
			[(names at: index) isNil ifTrue: [^0].
			index := index \\ names size + 1].
	^index</body>

<body package="Dictionary Framework" selector="extend">extend
	"extend implementation by doubling the number of pairs"
	
	| oldNames oldValues newSize |
	oldNames := names.
	oldValues := values.
	newSize := names size * 2.
	names := Array new: newSize.
	values := Array new: newSize.
	size := 0.
	1
		to: oldNames size
		do:
			[:index | 
			| name |
			name := oldNames at: index.
			name isNil
				ifFalse:
					[self
						at: name
						put: (oldValues at: index)]]</body>
</methods>

<methods>
<class-id>FastDictionary</class-id> <category>statistics</category>

<body package="Dictionary Framework" selector="size">size
	"dictionary size"
	
	^size</body>
</methods>


<methods>
<class-id>SmallDictionary</class-id> <category>private</category>

<body package="Dictionary Framework" selector="newIndexAtKey:">newIndexAtKey: key
	"locate index for new key in names"
	
	(self indexAtKey: key) = 0
		ifTrue:
			[self extend.
			names
				at: names size
				put: key.
			^names size]
		ifFalse: [^0]</body>

<body package="Dictionary Framework" selector="indexAtKey:">indexAtKey: key
	"locate index of key in names"
	
	1
		to: names size
		do: [:index | (names at: index) = key ifTrue: [^index]].
	^0</body>

<body package="Dictionary Framework" selector="extend">extend
	"extend implementation by one pair"
	
	| oldNames oldValues newSize |
	oldNames := names.
	oldValues := values.
	newSize := names size + 1.
	names := Array new: newSize.
	values := Array new: newSize.
	1
		to: oldNames size
		do:
			[:index | 
			names
				at: index
				put: (oldNames at: index).
			values
				at: index
				put: (oldValues at: index)]</body>
</methods>

<methods>
<class-id>SmallDictionary</class-id> <category>statistics</category>

<body package="Dictionary Framework" selector="size">size
	"dictionary size"
	
	^names size</body>
</methods>



</st-source>
