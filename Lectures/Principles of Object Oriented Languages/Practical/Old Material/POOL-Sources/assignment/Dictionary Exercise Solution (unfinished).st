<?xml version="1.0"?>

<st-source>
<time-stamp>From VisualWorks® NonCommercial, 7.5 of April 16, 2007 on January 18, 2008 at 5:13:53 pm</time-stamp>
<!-- Package (none)* -->


<component-property>
<name>(none)</name> <type>package</type>
<property>comment</property> <value>'This "package" is a special package that holds all code (classes, methods, shares, namespaces) that does not show up in a normal package. It is generated automatically. Removing it will remove the code contained in it, but the package itself never goes away. It can serve as a temporary "scratch space" to try quick throw away work before creating a normal package.'</value>
</component-property>

<class>
<name>TreeNode</name>
<environment>Smalltalk</environment>
<super>Core.Object</super>
<private>false</private>
<indexed-type>none</indexed-type>
<inst-vars>leftChild decisionKey rightChild </inst-vars>
<class-inst-vars></class-inst-vars>
<imports></imports>
<category>(none)</category>
<attributes>
<package>(none)</package>
</attributes>
</class>

<class>
<name>TreeLeaf</name>
<environment>Smalltalk</environment>
<super>Core.Object</super>
<private>false</private>
<indexed-type>none</indexed-type>
<inst-vars>storedKey storedValue </inst-vars>
<class-inst-vars></class-inst-vars>
<imports></imports>
<category>(none)</category>
<attributes>
<package>(none)</package>
</attributes>
</class>

<class>
<name>MyDictionary</name>
<environment>Smalltalk</environment>
<super>Core.Object</super>
<private>false</private>
<indexed-type>none</indexed-type>
<inst-vars>root </inst-vars>
<class-inst-vars></class-inst-vars>
<imports></imports>
<category>(none)</category>
<attributes>
<package>(none)</package>
</attributes>
</class>

<class>
<name>DummyTreeRootNode</name>
<environment>Smalltalk</environment>
<super>Core.Object</super>
<private>false</private>
<indexed-type>none</indexed-type>
<inst-vars></inst-vars>
<class-inst-vars></class-inst-vars>
<imports></imports>
<category>(none)</category>
<attributes>
<package>(none)</package>
</attributes>
</class>





<methods>
<class-id>DummyTreeRootNode</class-id> <category>accessing</category>

<body package="(none)" selector="contains:">contains: key

	^ false</body>

<body package="(none)" selector="at:put:withYourParent:">at: keyToStore put: valueToStore withYourParent: myParent

	myParent replaceChild: self with: (TreeLeaf newWithKey: keyToStore value: valueToStore)</body>

<body package="(none)" selector="at:">at: key

	^ nil
</body>

<body package="(none)" selector="remove:">remove: key

	self error: 'Key ' , key asString, ' does not exist in dictionary'.</body>
</methods>


<methods>
<class-id>TreeNode class</class-id> <category>instance creation</category>

<body package="(none)" selector="newWithKey:leftChild:rightChild:">newWithKey: aKey leftChild: theLeftChild rightChild: theRightChild

	^ self new initializeWithKey: aKey leftChild: theLeftChild rightChild: theRightChild</body>

<body package="(none)" selector="newToIndex:and:">newToIndex: child1 and: child2

	( child1 rightMostKey &lt; child2 leftMostKey ) ifTrue: [
		^ self newWithKey: child1 rightMostKey leftChild: child1 rightChild: child2.
	].
	( child2 rightMostKey &lt; child1 leftMostKey) ifTrue: [
		^ self newWithKey: child2 rightMostKey leftChild: child2 rightChild: child2.
	].
	self error: 'Cannot create node to index two trees with overlapping key ranges'.
</body>
</methods>


<methods>
<class-id>TreeNode</class-id> <category>initialize-release</category>

<body package="(none)" selector="initializeWithKey:leftChild:rightChild:">initializeWithKey: aKey leftChild: theLeftChild rightChild: theRightChild

	decisionKey := aKey.
	leftChild := theLeftChild.
	rightChild := theRightChild.
</body>
</methods>

<methods>
<class-id>TreeNode</class-id> <category>accessing</category>

<body package="(none)" selector="removeKey:withYourParent:">removeKey: key withYourParent: myParent

	^ self removeKey: key withYourParent: myParent grandparent: nil</body>

<body package="(none)" selector="removeKey:withYourParent:grandparent:">removeKey: key withYourParent: myParent grandparent: myGrandparent

	^ ((key &lt;= decisionKey) ifTrue: [ leftChild  ] ifFalse: [ rightChild ]) removeKey: key withYourParent: self grandparent: myParent


</body>

<body package="(none)" selector="rightMostKey">rightMostKey

	^ rightChild rightMostKey</body>

<body package="(none)" selector="at:put:withYourParent:">at: keyToStore put: valueToStore withYourParent: myParent

	( keyToStore &lt;= decisionKey ifTrue: [ leftChild ] ifFalse: [ rightChild ]) at: keyToStore put: valueToStore withYourParent: self


</body>

<body package="(none)" selector="at:">at: keyToFind

	^ (keyToFind &lt;= decisionKey ifTrue: [ leftChild ] ifFalse: [ rightChild ]) at: keyToFind


</body>

<body package="(none)" selector="leftMostKey">leftMostKey

	^ leftChild leftMostKey</body>
</methods>

<methods>
<class-id>TreeNode</class-id> <category>private</category>

<body package="(none)" selector="replaceChild:with:">replaceChild: aChild with: newChild

	( aChild == leftChild ) ifTrue: [ leftChild := newChild. ^ self ].
	( aChild == rightChild ) ifTrue: [ rightChild := newChild. ^ self ].
	self error: 'Attempt to replace a child that does not belong to this node.'

</body>
</methods>


<methods>
<class-id>TreeLeaf class</class-id> <category>instance creation</category>

<body package="(none)" selector="newWithKey:value:">newWithKey: aKey value: aValue

	^ self new initializeWithKey: aKey value: aValue</body>
</methods>


<methods>
<class-id>TreeLeaf</class-id> <category>initialize-release</category>

<body package="(none)" selector="initializeWithKey:value:">initializeWithKey: aKey value: aValue

	storedKey := aKey.
	storedValue := aValue.</body>
</methods>

<methods>
<class-id>TreeLeaf</class-id> <category>accessing</category>

<body package="(none)" selector="rightMostKey">rightMostKey

	^ storedKey</body>

<body package="(none)" selector="at:put:withYourParent:">at: keyToStore put: valueToStore withYourParent: myParent

	( keyToStore = storedKey ) ifTrue: [
		storedValue := valueToStore.
	] ifFalse: [
		myParent
			replaceChild: self
			with: (TreeNode newNodeToIndex: self and: (TreeLeaf newWithKey: keyToStore value: valueToStore))
	]

</body>

<body package="(none)" selector="at:">at: keyToFind

	^ ( keyToFind = storedKey ) ifTrue: [
		storedValue
	] ifFalse: [
		nil
	]</body>

<body package="(none)" selector="leftMostKey">leftMostKey

	^ storedKey</body>
</methods>


<methods>
<class-id>MyDictionary class</class-id> <category>instance creation</category>

<body package="(none)" selector="newDictionary">newDictionary

	^ self new initialize</body>
</methods>


<methods>
<class-id>MyDictionary</class-id> <category>initialize-release</category>

<body package="(none)" selector="initialize">initialize

	root := DummyTreeRootNode new.</body>
</methods>

<methods>
<class-id>MyDictionary</class-id> <category>accessing</category>

<body package="(none)" selector="at:">at: keyToFind

	^ root at: keyToFind</body>

<body package="(none)" selector="at:put:">at: aKey put: aValue

	root at: aKey put: aValue withYourParent: self
</body>

<body package="(none)" selector="remove:">remove: key

	root remove: key withYourParent: self</body>
</methods>

<methods>
<class-id>MyDictionary</class-id> <category>private</category>

<body package="(none)" selector="replaceChild:with:">replaceChild: someChild with: newChild

	(someChild == root) ifFalse: [ self error: 'Object to be replaced is not a child of this dictionary node.' ].
	
	root := newChild.
</body>

<body package="(none)" selector="removeChild:">removeChild: someChild

	(someChild == root) ifFalse: [ self error: 'Attempt to remove child that is not a child of this node.' ].
	
	root := DummyTreeRootNode new.</body>
</methods>



</st-source>
