{\rtf1\ansi\ansicpg1252\cocoartf949\cocoasubrtf350
{\fonttbl\f0\fswiss\fcharset0 Helvetica;\f1\fnil\fcharset0 Monaco;}
{\colortbl;\red255\green255\blue255;}
\paperw11900\paperh16840\margl1440\margr1440\vieww12960\viewh13140\viewkind1
\pard\tx565\tx1133\tx1700\tx2266\tx2832\tx3401\tx3967\tx4535\tx5102\tx5669\tx6235\tx6802\qc

\f0\b\fs40 \cf0 Programming in Smalltalk:\
Dictionaries and Binary Trees\
(part II)\
\pard\tx565\tx1133\tx1700\tx2266\tx2832\tx3401\tx3967\tx4535\tx5102\tx5669\tx6235\tx6802\ql\qnatural

\b0\fs24 \cf0 \
\pard\pardeftab720\qj
\cf0 In this part of this exercise you should integrate your new kind of dictionary into the dictionary framework discussed in the lectures (lecture 1, slides 47-58). First load in the code of this framework into your Smalltalk environment. Study the framework as it is for a moment before you go on to integrating your own kind of dictionary.\
\
\pard\tx565\tx1133\tx1700\tx2266\tx2832\tx3401\tx3967\tx4535\tx5102\tx5669\tx6235\tx6802\li480\fi-480\ql\qnatural

\b\fs26 \cf0 1
\fs28 .
\fs24  
\b0 The framework will need some redesigning to fit in your new kind of dictionary. The current root class of the framework for example expects subclasses to implement an 
\f1 indexAtKey:
\f0  message, which doesn't fit a tree-based dictionary. How will you restructure the framework to fit in your dictionary class?\
\pard\pardeftab720\qj
\cf0 Do the necessary restructuring of the existing framework, and integrate your tree-based dictionary into it. Extend your dictionaries with new messages if necessary to support the requirements of the framework.\
\
\pard\tx565\tx1133\tx1700\tx2266\tx2832\tx3401\tx3967\tx4535\tx5102\tx5669\tx6235\tx6802\li480\fi-480\ql\qnatural

\b\fs26 \cf0 2
\fs28 .
\fs24  
\b0 Can you identify a few places in the new framework and your implementation of part 1 of this exercise where 
\i polymorphism
\i0  is exploited? \
\
\pard\tx565\tx1133\tx1700\tx2266\tx2832\tx3401\tx3967\tx4535\tx5102\tx5669\tx6235\tx6802\ql\qnatural
\cf0 \
\
\
\
\
\
\
\
\
\
\
\
\
\pard\tx565\tx1133\tx1700\tx2266\tx2832\tx3401\tx3967\tx4535\tx5102\tx5669\tx6235\tx6802\li480\fi-480\ql\qnatural
\cf0 \
\pard\pardeftab720\qj
\cf0 \
\pard\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\ql\qnatural
\cf0 {{\NeXTGraphic Reflections.pdf \width1660 \height320
}�}\pard\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\ql\qnatural

\fs20 \cf0 \
\pard\pardeftab720\li260\qj

\fs22 \cf0 Restructering code without changing its behavior is also called 
\i refactoring
\i0 , something you often need to do before actually extending some existing system with some new behavior. There's a book available with a catalog of commonly applicable 
\i refactorings 
\i0 and the VisualWorks class browser has some support for automating these. It is best to ignore this for the moment for this exercise, but if you have some time left afterwards, you can explore this refactoring support a bit to see how you could have used it to accomplish the same. The refactorings most useful to you in this exercise can be found in the class browser in the following menus: 
\i Class > Other > Convert to Sibling
\i0  and 
\i Method > Refactoring > Push Up
\i0 .\
}