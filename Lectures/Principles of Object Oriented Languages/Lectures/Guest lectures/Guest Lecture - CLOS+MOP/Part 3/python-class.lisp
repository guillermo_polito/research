(in-package :mop-user)

(defclass python-object (standard-object)
  ((slot-table :initform (make-hash-table)
               :reader slot-table))
  (:documentation
   "The class that each python class must inherit from."))

(defclass python-class (standard-class)
  ()
  (:documentation
   "The metaclass for python classes."))

(defmethod initialize-instance :around
  ((class python-class)
   &rest initargs
   &key direct-superclasses)
  "Add python-object to the list of superclasses."
  (apply (function call-next-method)
         class
         :direct-superclasses
         (append direct-superclasses
                 (list (find-class 'python-object)))
         initargs))

(defmethod reinitialize-instance :around
  ((class python-class)
   &rest initargs
   &key (direct-superclasses '() direct-superclasses-p))
  "Add python-object to the list of superclasses."
  (if direct-superclasses-p
      ;; We only need to do something if someone actually
      ;; tries to change the list of direct superclasses.
      (apply (function call-next-method)
             class
             :direct-superclasses
             (append direct-superclasses
                     (list (find-class 'python-object)))
             initargs)
    (call-next-method)))

(defmethod slot-value-using-class
           ((class python-class) object slot)
  (if (eq slot 'slot-table)
      ;; The slot table must be handled as in plain CLOS.
      (call-next-method)
    (multiple-value-bind
        (value foundp)
        (gethash slot (slot-table object))
      (if foundp value
        (slot-unbound class object slot)))))

(defmethod (setf slot-value-using-class)
           (new-value (class python-class) object slot)
  (if (eq slot 'slot-table)
      ;; The slot table must be handled as in plain CLOS.
      (call-next-method)
    (setf (gethash slot (slot-table object))
          new-value)))

(defmethod slot-boundp-using-class
           ((class python-class) object slot)
  (if (eq slot 'slot-table)
      ;; The slot table must be handled as in plain CLOS.
      (call-next-method)
    (multiple-value-bind
        (value foundp)
        (gethash slot (slot-table object))
      (declare (ignore value))
      foundp)))

(defmethod slot-makunbound-using-class
           ((class python-class) object slot)
  (if (eq slot 'slot-table)
      ;; The slot table must be handled as in plain CLOS.
      (call-next-method)
    (remhash slot (slot-table object))))
