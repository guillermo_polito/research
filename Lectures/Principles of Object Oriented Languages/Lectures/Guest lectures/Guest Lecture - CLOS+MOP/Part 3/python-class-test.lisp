(in-package :mop-user)

(defclass person ()
  ()
  (:metaclass python-class))

(defparameter *pascal*
  (make-instance 'person))

(setf (slot-value *pascal* 'name) "Pascal")
(setf (slot-value *pascal* 'address) "Brussels")

(print (slot-value *pascal* 'name))
(print (slot-value *pascal* 'address))
