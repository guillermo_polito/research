(in-package :mop-user)

(defclass standard-class (cl:standard-class)
  ())

(defmethod validate-superclass
           ((class standard-class)
            (superclass cl:standard-class))
  t)

(defmethod initialize-instance :around
  ((class standard-class)
   &rest initargs
   &key direct-superclasses
   &allow-other-keys)
  (declare (dynamic-extent initargs))
  (if direct-superclasses
      (if (eq (car (last direct-superclasses))
              (find-class 'standard-object))
          (apply #'call-next-method class
                 :optimize-slot-access nil
                 initargs)
        (apply #'call-next-method class
               :direct-superclasses
               (remove (find-class 'standard-object)
                       direct-superclasses)
               :optimize-slot-access nil
               initargs))
    (apply #'call-next-method class
           :direct-superclasses
           (list (find-class 'standard-object))
           :optimize-slot-access nil
           initargs)))

(defmethod reinitialize-instance :around
           ((class standard-class)
            &rest initargs
            &key (direct-superclasses '() direct-superclasses-p)
            &allow-other-keys)
  (declare (dynamic-extent initargs))
  (if direct-superclasses-p
      (if direct-superclasses
          (if (eq (car (last direct-superclasses))
                  (find-class 'standard-object))
              (apply #'call-next-method class
                     :optimize-slot-access nil
                     initargs)
            (apply #'call-next-method class
                   :direct-superclasses
                   (remove (find-class 'standard-object)
                           direct-superclasses)
                   :optimize-slot-access nil
                   initargs))
        (apply #'call-next-method class
               :direct-superclasses
               (list (find-class 'standard-object))
               :optimize-slot-access nil
               initargs))
    (apply #'call-next-method class
           :optimize-slot-access nil
           initargs)))
