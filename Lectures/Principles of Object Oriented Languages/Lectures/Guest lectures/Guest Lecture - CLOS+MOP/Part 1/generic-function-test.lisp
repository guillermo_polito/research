(in-package :closless)

;; 'person is a subclass of 'object
;; with the slots 'name and 'address.

(setf (find-class 'person)
      (make-class :direct-superclass 'object
                  :direct-slots '(name address)))

;; 'employee is a subclass of 'person
;; with the additional slot 'employer.

(setf (find-class 'employee)
      (make-class :direct-superclass 'person
                  :direct-slots '(employer)))

;; Pascal is a 'person
;; with the name "Pascal" and the address "Brussels".

(defparameter *pascal*
  (make-object :class 'person))
(setf (slot-value *pascal* 'name) "Pascal")
(setf (slot-value *pascal* 'address) "Brussels")

;; Display is a generic function.

(defparameter <display>
  (make-generic-function))

;; The display method for 'person
;; prints the name and the address of a 'person.

(add-method
 <display>
 (make-method
  :specializer 'person
  :function (lambda (receiver args next-methods)
              (print (slot-value receiver 'name))
              (print (slot-value receiver 'address)))))

;; Let's call display on Pascal.

(call-generic-function <display> *pascal*)

;; We change the class of Pascal to 'employee
;; and set his employer to "Vrije Universiteit Brussel".

(setf (object-class *pascal*) 'employee)
(setf (slot-value *pascal* 'employer) "Vrije Universiteit Brussel")

;; The display method for 'employee
;; performs a super call and then
;; prints the employer of an 'employee.

(add-method
 <display>
 (make-method
  :specializer 'employee
  :function (lambda (receiver args next-methods)
              (call-next-method receiver args next-methods)
              (print (slot-value receiver 'employer)))))

;; Let's call display on Pascal again.

(call-generic-function <display> *pascal*)
